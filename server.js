const http = require('http');
const express = require('express');

app.use(express.static('public'));

const server = http.createServer(app);

app.listen(8180, () => console.log('Server is up and running'));